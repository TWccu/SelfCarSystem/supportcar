import json
import socket
import sys
import threading

import paho.mqtt.client as mqtt
from car_state import CarState
from find_line import SelfCarEyes
from led_controller import SevenSegmentDisplay
from motor_controller import CCUPiMotor_4Motor

carState = CarState()
# mc = CCUPiMotor(13, 11, 18, 16)
mc = CCUPiMotor_4Motor(38, 40, 35, 36, 32, 33, 29, 31)

autoController = SelfCarEyes(carState, mc)

if(len(sys.argv) > 1):
        if (sys.argv[1]=="remote"):
            carState.set_state(0)
else:
    carState.set_state(1)

sevenSegmentDisplay0 = SevenSegmentDisplay(36, 38, 40)
#open_door_thread = threading.Thread(target = autoController.open_door, args=(int(msgs_array[1]),))  ##異步


with open('selfCarConfig.json') as f:
    data = json.load(f)
    mqtt_in_topic = data["mqtt_in_topic"]
    print("mqtt_in_topic: ", mqtt_in_topic)
    mqtt_out_topic = data["mqtt_out_topic"]
    print("mqtt_out_topic: ", mqtt_out_topic)
    # mqtt_broker = data["mqtt_broker"]
    # print("mqtt_broker: ", mqtt_broker)
    # mqtt_port = data["mqtt_port"]
    # print("mqtt_port: ", mqtt_port)
    
client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
client.bind(("", 23130))
data, addr = client.recvfrom(1024)
payload = json.loads(data.decode('utf-8'))
mqtt_broker = payload['mqttip']
mqtt_port = payload['mqttport']
print('MQTT Server ' + mqtt_broker + ':' + str(mqtt_port))

def on_connect(client, userData, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe(mqtt_in_topic)


'''
切割字串，字串格式為 { command,value } 
command有state, m, Connect, door，on_message中有定義各個對應的動作
'''
def spilt_message(msgs):
    return msgs.split(',')


'''
state,-1 詢問目前狀態
state,1 設定狀態為1(自動模式)
state,0 設定狀態為0(遙控模式){目前無用}
'''
def state_center(args):
    args = int(args)
    if args == -1:
        msg = 'state,'+ str(carState.get_state())
        response_mqtt(msg)
    elif args == 1:
        carState.set_state(1)
        response_mqtt('OK')
    elif args == 0:
        carState.set_state(0)
        response_mqtt('OK')


'''
m,l,50,r,100 設定馬達速度，左輪50右輪100
args在on_message就會做切割，數入此method時會變成 ['m', '50', 'r', '100']
'''
def remote_center(args):
    if (carState.get_state() != 0):
        response_mqtt('error,please set state to 0')
        return
    mc.setSpeed(float(args[1]), float(args[3]))


'''用來發送mqtt訊息而已'''
def response_mqtt(msgs):
    client.publish(mqtt_out_topic, msgs)


'''處理各種字定義message'''
def on_message(client, userData, msg):
    msgs = msg.payload.decode("utf8")
    msgs_array = spilt_message(msgs)
    print(msgs)
    print(msgs_array[0])
    print(carState.get_state())
    if msgs_array[0] == 'state':
        state_center(msgs_array[1])
    elif msgs_array[0] == 'm':
        remote_center(msgs_array[1:5])
    elif msgs_array[0] =='Connect':
        response_mqtt(carState.get_state())
    elif msgs_array[0] == "door":
        sevenSegmentDisplay0.display(int(msgs_array[1]))
        if carState.get_state() == 1:
            open_door_thread = threading.Thread(target = autoController.open_door, args=(int(msgs_array[1]),))  ##異步
            open_door_thread.start()
            print('go~')
        


if __name__ == '__main__':
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(mqtt_broker, mqtt_port, 60)
    client.loop_forever()
