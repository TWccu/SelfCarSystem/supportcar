import time

import cv2
import num_detector as num_detector
import numpy as np
from picamera import PiCamera
from picamera.array import PiRGBArray


class SelfCarEyes:

    def __init__(self, car_state=None, ccuPiMotor=None):
        self.forward = 1
        self.isTurnRight = 0
        self.ccuPiMotor = ccuPiMotor
        self.car_state = car_state
        self.theta = 0
        self.minLineLength = 100
        self.maxLineGap = 10
        self.height = 480
        self.weight = 640
        self.hasGate = 0
        self.counter = 0
        self.turnState = 0
        self.missionState = 0
        self.camera = PiCamera()
        self.camera.resolution = (self.weight, self.height)
        self.camera.framerate = 30
        self.rawCapture = PiRGBArray(self.camera, size=(self.weight, self.height))
        time.sleep(0.1)

    def init_camera(self):
        self.rawCapture.truncate(0)
        time.sleep(0.1)

    def open_door(self, num):
        self.init_camera()
        for frame in self.camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True):

            if self.car_state.get_state() == 0:
                self.ccuPiMotor.stop()
                self.missionState = 0
                return

            camera = cv2.GaussianBlur(frame.array[:, :], (5,5), 0)
            gateImage = frame.array[:240, 420:]
#            cv2.imshow("gateImage", gateImage)
            camera = cv2.cvtColor(camera, cv2.COLOR_BGR2GRAY)
            seeingNum = num_detector.findNum(gateImage, 500, 3000)
            print(seeingNum)
            if self.missionState == 0 and seeingNum == num:
                self.missionState = 1
                self.forward = -1
                self.ccuPiMotor.stop()
                time.sleep(15)
            elif seeingNum == 0 and self.missionState == 1:
                self.forward = 1
                self.missionState = 0
                self.ccuPiMotor.stop()
                cv2.destroyAllWindows()
                return
##            roadImage = camera[350:, :]
            roadImage = camera[290:410, :360]
##            cv2.imshow("roadImage", roadImage)
            roadImage = cv2.threshold(roadImage, 127, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

            self.followLine(roadImage=roadImage)

            self.rawCapture.truncate(0)

    def followLine(self, roadImage):
        right = np.sum(roadImage[:80, 260:], dtype=int)
        left = np.sum(roadImage[80:, :100], dtype=int)
        if True:
            if (right - left) > 8000:
                self.isTurnRight = 3
                if (self.turnState != 1):
                    self.turnState = 1
                    self.counter = 0
                self.ccuPiMotor.setSpeed(55 + self.counter, 0 - self.counter)
                self.counter += 0.1
                if self.counter > 45:
                    self.counter = 45
            elif (right - left) < -8000:
                if self.isTurnRight > 0:
                    self.isTurnRight -= 1
                    self.ccuPiMotor.stop()
                else:
                    if (self.turnState != -1):
                        self.turnState = -1
                        self.counter = 0
                    self.ccuPiMotor.setSpeed(0 - self.counter, 50 + self.counter)
                    self.counter += 0.1
                    if self.counter > 45:
                        self.counter = 45
            else:
                if self.isTurnRight > 0:
                    self.isTurnRight -= 1
                    self.ccuPiMotor.stop()
                else:
                    self.counter = 0
                    self.ccuPiMotor.setSpeed(55, 55)